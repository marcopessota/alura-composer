<?php

namespace Alura\BuscadorDeCursos;

use GuzzleHttp\ClientInterface;
use Symfony\Component\DomCrawler\Crawler;

class Seeker
{
    private $httpClient;
    private $crawler;

    public function __construct(ClientInterface $httpClient, Crawler $crawler)
    {
        $this->httpClient = $httpClient;
        $this->crawler = $crawler;
    }

    public function seek(string $uri): array
    {

        $response = $this->httpClient->request('GET', $uri);

        $html = $response->getBody();

        $this->crawler->addHtmlContent($html);

        $courses = [];
        $coursesElements = $this->crawler->filter('span.card-curso__nome');
        foreach ($coursesElements as $courseElement) {
            $courses[] = $courseElement->textContent;
        }
        return $courses;
    }
}
