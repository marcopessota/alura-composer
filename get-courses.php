#!/usr/bin/env php
<?php
require 'vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Alura\BuscadorDeCursos\Seeker;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

$Client = new Client(['base_uri' => 'https://www.alura.com.br/']);
$Crawler = new Crawler();

$Seeker = new Seeker($Client, $Crawler);
$courses = $Seeker->seek('cursos-online-programacao/php');
foreach ($courses as $course) {
    echo $course . PHP_EOL;
}